#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include "DHT.h"


//---Piny---
//DIGITAL
int Sgleba = 1;               // Analogowe wyjscie czujnika wilgotnosci gleby
#define DHTPIN 2            // Czujnik wilgotnosci powietrza
int WentWdm = 3;              // Wentylator wdmuchujący powietrze
//int WentWydm = 4;           // Wentylator wydmuchujący powietrze
int Lampa = 5;                // Lampa
int Grzalka = 6;              // Grzałki
int Pompa = 7;                // Pompka
int Bup = 8;                  // Przycisk góra
int Bdown = 9;                // Przycisk dół
int Benter = 10;              // Przycisk ENTER
int Poziom_Wody = 11;       // Poziom wody
int Grzalka_Woda = 4;       // Grzalka woda
//ANAGLO
int TempLM35Pin = A3;         // Czujnik temperatury
int SglebaA = A0;             // Czujnik wilgotności gleby
int TempLM45Pin_Woda = A1;  // czujnik temperatury wody
                            // dodatkowo wyswietlacz przez I2C - A5 i A4


// Regulator temperatura
#define ODSTEP_T 1000                      // czas pomiedzy pomiarami 5min - 300.000ms
#define LIMIT_ODCZYTOW_T 5                // limit po ilu odczytach liczmy srednia
unsigned long czas_poprzedni_T = 0;
float HistTemp = 1;
float SumaTemp = 0;                         // zmienna pomocnicza do liczenia sredniej
int liczba_odczytow_T = 0;
float SredniaT = 0;
int LM35_Odczyt = 0;
//---regulator wilgotnosc
#define ODSTEP_W 60000//300000                     // czas pomiedzy pomiarami 5min - 300.000ms
#define LIMIT_ODCZYTOW_W 30               // limit po ilu odczytach liczmy srednia
unsigned long czas_poprzedni_W = 0;
int SumaWilg = 0;
int SredniaW = 0;
int liczba_odczytow_W = 0;
// Temperatura wody
unsigned long czas_aktualny = 0;
unsigned long czas_poprzedni_TW = 0;
int liczba_odczytow_TW = 20;
int ODSTEP_TW = 500;
int LIMIT_ODCZYTOW_TW = 5;
float SumaTempW = 0;
float SredniaTW = 0;

// Grzalka woda
unsigned long czas_poprzedni_Grzalka_Woda = 0;
unsigned long ODSTEP_Grzalka_Woda = 600000;

//--wentylatory
unsigned long ODSTEP_Wen = 1800000;       // 30min
unsigned long czas_poprzedni_Wen = 0;

//--Lampa
unsigned long czas_poprzedni_Lampa = 0;//14390000;

//--Pompa
unsigned long czas_poprzedni_Pompa = 0;

//--Wyswietlacz
#define ODSTEP_WYS 250                    //czestotliwosc odswierzania wyswietlacza
unsigned long czas_poprzedni_Wys = 0;

//menu--
#define ODSTEP_MENU 250
unsigned long czas_poprzedni_Menu = 0;

//uruchomienie
unsigned long czas_uruchomienia = 0;

// Wartosci zadane
float TempWodZad = 50;
int WilgPowZad = 30;
float WilgZad = 90;//250;
float TempZad = 26;
int DlugoscPompa = 10000;
int NaswietlanieMin = 0;
int NaswietlanieGodz = 20;
unsigned long DlugoscWent = 0;

//---GLOBALNE--
// bufory
int Menu = 1;
int pMenu = 1;
float BuforF = 0;                         // bufor pomocniczy do obslugi menu typu FLOAT
int BuforI = 0;                           // bufor pomocniczy do obslugi menu typu INT
int j = 0;
unsigned long czas_poprzedni_Wyswietl = 0;

// markery
bool ENTER_M = 0;
bool ENTER_pM = 0;
bool ZMIANA = 0;
bool ZMIANA_pM = 0;
bool bMarker_pM = 0;
bool WENTY_M = 0;
bool WENTY_M1 = 0;
bool LAMPA_M = 0;
bool LAMPA_M1 = 0;
bool PIERWSZE = 1;


LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

#define DHTTYPE DHT11   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);   // Inicjalizacja LCD 2x16
  lcd.backlight(); // zalaczenie podwietlenia 

  //wejscia
  pinMode(Grzalka, OUTPUT);
  digitalWrite(Grzalka, HIGH);
  pinMode(WentWdm, OUTPUT);
  digitalWrite(WentWdm, HIGH);
  //pinMode(WentWydm, OUTPUT);
  //digitalWrite(WentWydm, HIGH);
  pinMode(Lampa, OUTPUT);
  digitalWrite(Lampa, HIGH);
  pinMode(Pompa, OUTPUT);
  digitalWrite(Pompa, LOW);
  pinMode(Grzalka_Woda, OUTPUT);
  digitalWrite(Grzalka_Woda, HIGH);

  //wyjscia
  pinMode(Sgleba, INPUT);
  pinMode(Bup, INPUT);
  pinMode(Bdown, INPUT);
  pinMode(Benter, INPUT);
  pinMode(Poziom_Wody, INPUT);
  dht.begin();
  
  SredniaT = 100;       // zeby nie grzalo na czas pomiarów
  SredniaTW = 100;      //
 

     
  //-- Inicjalizacja programu--
  lcd.setCursor(0,0); // Ustawienie kursora w pozycji 0,0 (pierwszy wiersz, pierwsza kolumna)
  lcd.print("--Wczytywanie--");
  //PETLA WCZYTYWANIA
  for(int i = 0; i < 16 ; i++) {
    lcd.setCursor(i,1); //Ustawienie kursora w pozycji 0,0 (drugi wiersz, pierwsza kolumna)
    lcd.print("#");
    delay(100);
  }
  czas_uruchomienia = millis();
}

void loop() {
  unsigned long Pozostalo, PozostaloS;
  int H,minuty,sek,Dni;
  
  unsigned long czas_aktualny = millis();       //czas

  //--DHT11--
  float Wilg = dht.readHumidity();         // wilgotność
  float TempDHT = dht.readTemperature();      // temperatura z DHT11
  
  // błąd dht
  if (isnan(Wilg) || isnan(TempDHT) ) {
    //Serial.println("Failed to read from DHT sensor!");
    Menu = 10;
  }
  float Cieplo = dht.computeHeatIndex(TempDHT, Wilg, false);
  //koniec DHT11
   
    //--LAMPA--
    if((unsigned long)(czas_aktualny - czas_poprzedni_Lampa) > ((unsigned long)NaswietlanieGodz * 60 * 60 * 1000 + (unsigned long)NaswietlanieMin * 60 * 1000) && LAMPA_M == 0 ) { 
        czas_poprzedni_Lampa = czas_aktualny ;
        Serial.println("OFF");
        LAMPA_M1 = 0;
        LAMPA_M = 1;
        PIERWSZE = 0;
        digitalWrite(Lampa, HIGH);
    }
    if( ( (unsigned long)(czas_aktualny - czas_poprzedni_Lampa) > (86400000 - ((unsigned long)NaswietlanieGodz * 60 * 60 * 1000 + (unsigned long)NaswietlanieMin * 60 * 1000) )  && LAMPA_M1 == 0  ) || PIERWSZE == 1 ) {
        czas_poprzedni_Lampa = czas_aktualny ;
        LAMPA_M1 = 1;
        LAMPA_M = 0;
        Serial.println("ON");
        digitalWrite(Lampa, LOW);
    }// koniec LAMPA
    
    //--WENTYLATORY--
    if((unsigned long)(czas_aktualny - czas_poprzedni_Wen > DlugoscWent) && WENTY_M == 0) {
        czas_poprzedni_Wen = czas_aktualny ;
        WENTY_M1 = 0;
        WENTY_M = 1;
        digitalWrite(WentWdm, HIGH);
        //digitalWrite(WentWydm,HIGH);
    }
    if(((unsigned long)(czas_aktualny - czas_poprzedni_Wen) > ODSTEP_Wen) && WENTY_M1 == 0) {
        czas_poprzedni_Wen = czas_aktualny ;
        WENTY_M1 = 1;
        WENTY_M = 0;
        digitalWrite(WentWdm, LOW);
        //digitalWrite(WentWydm, LOW);
    }//--koniec WENTY--
    
   
        
   //----TEMPERATURA----
   // pomiary
   if((unsigned long)(czas_aktualny - czas_poprzedni_T) > ODSTEP_T) {
      czas_poprzedni_T = czas_aktualny; 
      liczba_odczytow_T = liczba_odczytow_T + 1;
      analogRead(TempLM35Pin);
      LM35_Odczyt = analogRead(TempLM35Pin);
      float mv = (LM35_Odczyt / 1024.0) * 5000; 
      float temperatureC = mv / 10;
      SumaTemp = SumaTemp + temperatureC; 
      if(liczba_odczytow_T >= LIMIT_ODCZYTOW_T){
        SredniaT = SumaTemp / liczba_odczytow_T;
        liczba_odczytow_T = SumaTemp = 0;
      }
  }// koniec pomiary
  
  // Regulator temperatury
  if(SredniaT > TempZad){
     digitalWrite(Grzalka, HIGH);
  }
  else if(SredniaT < (TempZad - HistTemp)){
     digitalWrite(Grzalka, LOW);
  }// koniec regulator temperatury

  //-- GLEBA--
    if((unsigned long)(czas_aktualny - czas_poprzedni_W) > ODSTEP_W) {
        czas_poprzedni_W = czas_aktualny;
        liczba_odczytow_W = liczba_odczytow_W + 1;
        SumaWilg = SumaWilg + analogRead(SglebaA);
        //Serial.print("suma!: ");

        //Serial.println(SumaWilg);
        //Serial.print("ilosc!: ");
        // Serial.println(liczba_odczytow_W);
        //Serial.print("Odczyt: ");
        if(liczba_odczytow_W >= LIMIT_ODCZYTOW_W){
          SredniaW = ceil(SumaWilg / liczba_odczytow_W);
          if (SredniaW > WilgZad) { 
              digitalWrite(Pompa,HIGH);
              czas_poprzedni_Pompa = czas_aktualny ;
          }
          //Serial.print("Średnia!: ");
          //Serial.println(SredniaW);
          liczba_odczytow_W = SumaWilg = 0;
        }
    }
    //PODLEWANIE
    if(czas_aktualny - czas_poprzedni_Pompa > DlugoscPompa) {
        digitalWrite(Pompa,LOW);
    } //--koniec GLEBA--

    // WILGOTNOSC POWIETRZA
    if(czas_aktualny - czas_poprzedni_TW > ODSTEP_TW) {
      czas_poprzedni_TW = czas_aktualny; 
      liczba_odczytow_TW = liczba_odczytow_TW + 1;
      float temperatureCW = ((analogRead(TempLM45Pin_Woda) / 1024.0) * 5000) / 10;
            //Serial.println(temperatureCW);
      SumaTempW = SumaTempW +temperatureCW;
      if(liczba_odczytow_TW >= LIMIT_ODCZYTOW_TW){
        SredniaTW = SumaTempW / liczba_odczytow_TW;
        liczba_odczytow_TW = SumaTempW = 0;
      }
    }// koniec pomiary

    if( (SredniaTW < TempWodZad) && (Wilg <= WilgPowZad) && (digitalRead(Poziom_Wody) == HIGH)){
      if(czas_aktualny - czas_poprzedni_Grzalka_Woda > ODSTEP_Grzalka_Woda) {
        digitalWrite(Grzalka_Woda, LOW);
      }
    }
    if( SredniaTW >= TempWodZad) {
      digitalWrite(Grzalka_Woda, HIGH);
      czas_poprzedni_Grzalka_Woda = czas_aktualny;
    }// koniec wilgotnosci powietrza

    //-------------------------------------------------
    //--------OBSLUGA PRZYCISKOW I WYSWIETLACZA--------
    //-------------------------------------------------
  // Filtrowanie wyswietlacza
  if(czas_aktualny - czas_poprzedni_Wys > ODSTEP_WYS) {
    czas_poprzedni_Wys = czas_aktualny;
    
    // PRZYCISKI
     if((digitalRead(Benter) == HIGH) && ZMIANA == 0 && ENTER_M == 0) {
      ENTER_M = 1;
      czas_poprzedni_Menu = czas_aktualny;
    }
    if(ENTER_M == 0 ) {
      if (digitalRead(Bup) == HIGH) {
        Menu++;
        if(Menu > 9 ) {
          Menu = 1; }
      }
      if (digitalRead(Bdown) == HIGH) {
        Menu--;
        if(Menu < 1) {
          Menu = 9; }
      }
    }// koniec przycisków
    
  //----MENU---  
    if(ENTER_M == 0 ) {
      switch(Menu) {
        case 1:
          BuforF = TempZad;
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Temperatura:");
          lcd.setCursor(0,1);
          lcd.print(SredniaT);
          lcd.setCursor(11,1);
          if ( digitalRead(Grzalka) == HIGH ) {
            lcd.print("OFF");
          }
          if ( digitalRead(Grzalka) == LOW ) {
            lcd.print("ON");
          }
          break; 
        case 2:
          BuforI = WilgPowZad;
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Wilgotnosc pow:");
          lcd.setCursor(0,1);
          lcd.print(Wilg);
          lcd.setCursor(11,1);
          break;
        case 3:
          BuforF = WilgZad;
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Wilgotnosc gleb:");
          lcd.setCursor(0,1);
          lcd.print(SredniaW);
          break;
        case 4:
          BuforF = TempWodZad;
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Temperat. wody:");
          lcd.setCursor(0,1);
          lcd.print(SredniaTW);
          if(digitalRead(Poziom_Wody) == LOW) {
            lcd.setCursor(6,1);
            lcd.print("Brak wody");
          }
          if(digitalRead(Poziom_Wody) == HIGH) {
            lcd.setCursor(11,1);
            if ( digitalRead(Grzalka_Woda) == HIGH ) {
            lcd.print("OFF");
            }
            if ( digitalRead(Grzalka_Woda) == LOW ) {
              lcd.print("ON");
            }
          }
          break;
        case 5:
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Naswietlenie:");
          lcd.setCursor(0,1);
          lcd.print("Godz:");
          lcd.setCursor(5,1);
          lcd.print(NaswietlanieGodz);
          lcd.setCursor(8,1);
          lcd.print("min:");
          lcd.setCursor(13,1);
          lcd.print(NaswietlanieMin);
          break;
        case 6:
          BuforI = DlugoscPompa;
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Pompa:");
          lcd.setCursor(0,1);
          lcd.print(DlugoscPompa/1000);
          lcd.setCursor(4,1);
          lcd.print("sekund");
          break;
        case 7:
          BuforI = DlugoscWent;
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Wentylatory:");
          lcd.setCursor(0,1);
          lcd.print(DlugoscWent/1000);
          lcd.setCursor(4,1);
          lcd.print("sekund");
          break;
       case 8:
          Pozostalo = (unsigned long ) ( czas_aktualny - czas_poprzedni_Lampa );
          PozostaloS = (unsigned long ) ( Pozostalo / 1000 );
          H = floor ( (PozostaloS / 3600) );
          minuty = floor ( ( ( PozostaloS - H * 3600) / 60 ) );
          sek = ((PozostaloS - H * 3600 - minuty * 60));
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Akt. czas nas.:");
          lcd.setCursor(0,1);
          lcd.print(H);                 //lcd.print(Dni);
          lcd.setCursor(2,1);
          lcd.print(":");
          lcd.setCursor(3,1);
          lcd.print(minuty);
          lcd.setCursor(5,1);
          lcd.print(":");
          lcd.setCursor(6,1);
          lcd.print(sek);
          lcd.setCursor(10,1);
          if ( digitalRead(Lampa) == HIGH ) {
            lcd.print("OFF");
          }
          if ( digitalRead(Lampa) == LOW ) {
            lcd.print("ON");
          }
          //lcd.print(":");
          //lcd.setCursor(10,1);
          //lcd.print(sek);
          break;
        case 9:
          PozostaloS = (unsigned long ) czas_aktualny / 1000;
          Dni = floor( (unsigned long ) PozostaloS / (86400) );
          H = floor( (unsigned long ) ( ( (unsigned long ) PozostaloS - ( (unsigned long ) Dni * 86400) ) / 3600) );
          minuty = floor ( ( ( (unsigned long ) PozostaloS - ( (unsigned long ) Dni * 86400) - ( (unsigned long ) H * 3600 ) ) / 60) );
          sek = ( ( (unsigned long ) PozostaloS - (unsigned long ) H * 3600 - (unsigned long ) minuty * 60 ) - ( (unsigned long ) Dni * 86400) );
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Akt. czas pracy:");
          lcd.setCursor(0,1);
          lcd.print(Dni);                 //lcd.print(Dni);
          lcd.setCursor(2,1);
          lcd.print(":");
          lcd.setCursor(3,1);
          lcd.print(H);
          lcd.setCursor(5,1);
          lcd.print(":");
          lcd.setCursor(6,1);
          lcd.print(minuty);
          lcd.setCursor(8,1);
          lcd.print(":");
          lcd.setCursor(10,1);
          lcd.print(sek);
          break;   
        case 10:
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Blad odczytu DHT");
          break;
        default:
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("ERROR");
          break;
      }
    }// koniec MENU
      
    // zmiana parametrow
    if(ENTER_M == 1 ) {

      // Obsłóga pod menu!
       if(czas_aktualny - czas_poprzedni_Menu > ODSTEP_MENU) {
         if((digitalRead(Benter) == HIGH) && ZMIANA_pM == 0) {
            ENTER_pM = 1;
          }
       }
          if(ENTER_pM == 0 ) {
            if (digitalRead(Bup) == HIGH) {
              pMenu++;
            if(pMenu > 3) {
              pMenu = 1; }
            }
            if (digitalRead(Bdown) == HIGH) {
              pMenu--;
            if(pMenu < 1) {
              pMenu = 3; }
            }
          }// koniec obsługi pod menu!
       
      
      // zmiana wartosci zadanej temperatury
      if(Menu == 1) {
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Temp. zadana:");
          lcd.setCursor(0,1);
          lcd.print(BuforF);
          
        if (digitalRead(Bup) == HIGH) {
          BuforF = BuforF + 0.5;
          ZMIANA = 1  ;        
        }
        if (digitalRead(Bdown) == HIGH) {
          BuforF = BuforF - 0.5;
          ZMIANA = 1;
        }
        if((digitalRead(Benter) == HIGH) && ZMIANA == 1) {
           TempZad = BuforF;
           ENTER_M = 0;
           Menu = 1;
           ZMIANA = 0;
        }
      }// koniec zmiany temperatury
      // zmiana wartosci zadanej wilgotnosci powietrza
      if(Menu == 2) {
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Wilg. pow. zadana:");
          lcd.setCursor(0,1);
          lcd.print(BuforI);
          
        if (digitalRead(Bup) == HIGH) {
          BuforI = BuforI + 1;
          ZMIANA = 1  ;        
        }
        if (digitalRead(Bdown) == HIGH) {
          BuforI = BuforI - 1;
          ZMIANA = 1;
        }
        if((digitalRead(Benter) == HIGH) && ZMIANA == 1) {
           WilgPowZad = BuforI;
           ENTER_M = 0;
           Menu = 2;
           ZMIANA = 0;
        }
      }// koniec zmiany wartosci zadanej wilgotnosci powietrza
      // zmiana wartosci zadanej wilgotnosci gleby
      if(Menu == 3) {
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Wilg. gleb. zadana:");
          lcd.setCursor(0,1);
          lcd.print(BuforF);
          
        if (digitalRead(Bup) == HIGH) {
          BuforF = BuforF + 10;
          ZMIANA = 1  ;        
        }
        if (digitalRead(Bdown) == HIGH) {
          BuforF = BuforF - 10;
          ZMIANA = 1;
        }
        if((digitalRead(Benter) == HIGH) && ZMIANA == 1) {
           WilgZad = BuforF;
           ENTER_M = 0;
           Menu = 3;
           ZMIANA = 0;
        }
      }// koniec zmiany wartosci zadanej wilgotnosci gleby
      // zmiana wartosci zadanej temperatury wody
      if(Menu == 4) {
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Temp. wody zad.:");
          lcd.setCursor(0,1);
          lcd.print(BuforF);
          
        if (digitalRead(Bup) == HIGH) {
          BuforF = BuforF + 1;
          ZMIANA = 1  ;      
        }
        if (digitalRead(Bdown) == HIGH) {
          BuforF = BuforF - 1;
          ZMIANA = 1;
        }
        if((digitalRead(Benter) == HIGH) && ZMIANA == 1) {
           WilgPowZad = BuforF;
           ENTER_M = 0;
           Menu = 4;
           ZMIANA = 0;
        }
      }// koniec zminany wartosci zadanej temperatury wody
      if(Menu == 5 || Menu == 8) {
          lcd.clear();
          lcd.setCursor(0,0); 
          lcd.print("Brak mozliwosci");
          lcd.setCursor(0,1);
          lcd.print("zmiany parametr.");
          if (digitalRead(Bup) == HIGH) {
            ZMIANA = 1 ;
          }
          if (digitalRead(Bdown) == HIGH) {
            ZMIANA = 1;
          }
          if((digitalRead(Benter) == HIGH) && ZMIANA == 1) {
            ENTER_M = 0;
            Menu = 1;
            ZMIANA = 0;
          }      
        }
          
      // zmiana czasu dzialania pompy
      if(Menu == 6) {
          // Podmenu dla pompy
          if ( ENTER_pM == 0 ) {
            switch( pMenu ) {
              case 1:
                BuforF = TempZad;
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Zmiana wartosci");
                lcd.setCursor(0,1);
                lcd.print("zadanej...");
                break; 
              case 2:
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Wlacz pompe...");
                czas_poprzedni_Pompa = czas_aktualny ;
                break;
              case 3:
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Wroc...");
                break;
              default:
                lcd.clear();
                lcd.setCursor(0,0);
                lcd.print("ERROR");
                break;
            }
          }// koniec podmenu pompy
          // zmiana wartości zadanej 
          if (ENTER_pM == 1 ){ 
          
            if( pMenu == 1 ) {
              lcd.clear();
              lcd.setCursor(0,0); 
              lcd.print("Pompa zadana:");
              lcd.setCursor(0,1);
              lcd.print(BuforI / 1000);
              lcd.setCursor(4,1);
              lcd.print("sekund");
              
              if (digitalRead(Bup) == HIGH) {
                BuforI = BuforI + 1000;
                ZMIANA = 1  ;
                ZMIANA_pM = 1  ;          
              }
              if (digitalRead(Bdown) == HIGH) {
                BuforI = BuforI - 1000;
                ZMIANA = 1;
                ZMIANA_pM = 1;
              }
              if((digitalRead(Benter) == HIGH) && ZMIANA_pM == 1) {
                 DlugoscPompa = BuforI;
                 ENTER_M = 0;
                 ENTER_pM = 0;
                 pMenu = 1;
                 Menu = 6;
                 ZMIANA = 0;
                 ZMIANA_pM = 0;
              }
            }
            // podlewanie
            if( pMenu == 2 ) {
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Podlewanie...");
                lcd.setCursor(0,1); 
                lcd.print("Pozostalo: ");
                lcd.setCursor(11,1); 
                lcd.print( ( DlugoscPompa - ( czas_aktualny - czas_poprzedni_Pompa ) ) / 1000 ); 
              if ( bMarker_pM == 0 ) {
                bMarker_pM == 1;
                digitalWrite(Pompa,HIGH);
                liczba_odczytow_W = SumaWilg = 0;
              }         
              if(czas_aktualny - czas_poprzedni_Pompa > DlugoscPompa) {
                digitalWrite(Pompa,LOW);
                bMarker_pM == 0;
                ENTER_M = 0;
                ENTER_pM = 0;
                pMenu = 1;
                Menu = 6;
              }
            }// Koniec podlewania
            if( pMenu == 3 ) {
                 DlugoscPompa = BuforI;
                 ENTER_M = 0;
                 ENTER_pM = 0;
                 pMenu = 1;
                 Menu = 6;
                 ZMIANA = 0;
            }
         }
      }// koniec zmiany czasu dzialania pompy
      // zmiana czasu dzialania wentylatorow
      if(Menu == 7) {
          if ( ENTER_pM == 0 ) {
            switch( pMenu ) {
              case 1:
                BuforI = DlugoscWent;
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Zmiana wartosci");
                lcd.setCursor(0,1);
                lcd.print("zadanej...");
                break; 
              case 2:
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Wlacz wenty...");
                czas_poprzedni_Wen = czas_aktualny ;
                break;
              case 3:
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Wroc...");
                break;
              default:
                lcd.clear();
                lcd.setCursor(0,0);
                lcd.print("ERROR");
                break;
            }
          }// koniec podmenu pompy
          // zmiana wartości zadanej 
          if (ENTER_pM == 1 ){ 
          
            if( pMenu == 1 ) {
              lcd.clear();
              lcd.setCursor(0,0); 
              lcd.print("Wenty. zadana:");
              lcd.setCursor(0,1);
              lcd.print(BuforI / 1000);
              lcd.setCursor(4,1);
              lcd.print("sekund");
              
              if (digitalRead(Bup) == HIGH) {
                BuforI = BuforI + 1000;
                ZMIANA = 1;        
              }
              if (digitalRead(Bdown) == HIGH) {
                BuforI = BuforI - 1000;
                ZMIANA = 1;
              }
              if((digitalRead(Benter) == HIGH) && ZMIANA == 1) {
                 DlugoscWent = BuforI;
                 ENTER_M = 0;
                 ENTER_pM = 0;
                 Menu = 7;
                 ZMIANA = 0;
                 ZMIANA_pM = 0;
              }
            }
            // wenty
            if( pMenu == 2 ) {
                lcd.clear();
                lcd.setCursor(0,0); 
                lcd.print("Chlodzenie...");
                lcd.setCursor(0,1); 
                lcd.print("Pozostalo: ");
                lcd.setCursor(11,1); 
                lcd.print( ( DlugoscWent - ( czas_aktualny - czas_poprzedni_Wen ) ) / 1000 ); 
                 if ( bMarker_pM == 0 ) {
                    bMarker_pM == 1;
                    digitalWrite(WentWdm,LOW);
                  }         
                if(czas_aktualny - czas_poprzedni_Wen > DlugoscWent) {
                  digitalWrite(WentWdm,HIGH);
                  bMarker_pM == 0;
                  ENTER_M = 0;
                  ENTER_pM = 0;
                  pMenu = 1;
                  Menu = 7;
                }
            }// Koniec wenty
            if( pMenu == 3 ) {
                 DlugoscPompa = BuforI;
                 ENTER_M = 0;
                 ENTER_pM = 0;
                 pMenu = 1;
                 Menu = 7;
                 ZMIANA = 0;
            }
         }
      }// koniec miany czasu dzialania wentylatorow 
    }// kociec MENU
  }// kociec odświerzanie
      
  // pomocnicze wyswietlanie w serialu
  // testy

  PIERWSZE = 0;
  if(czas_aktualny - czas_poprzedni_Wyswietl > 1000) {
    czas_poprzedni_Wyswietl = czas_aktualny;
   //Serial.println(86400000 - ((unsigned long)NaswietlanieGodz * 60 * 60 * 1000 + (unsigned long)NaswietlanieMin * 60 * 1000));
     //Serial.println(((unsigned long)NaswietlanieGodz * 60 * 60 * 1000 + (unsigned long)NaswietlanieMin * 60 * 1000));
Serial.println((czas_aktualny - czas_poprzedni_Lampa)/1000);
    /* Serial.print(j);
     Serial.print(" Temperatura: ");
     Serial.print(SredniaT);
     Serial.print(" Wilgotnosc ");
     Serial.print(SredniaW);
     Serial.print(";");
     Serial.print(SredniaTW);
     Serial.print("\n");
     j++;*/
  }
}
